import { Injectable } from '@angular/core';
import {Cars, Athletes} from './data/data';

@Injectable({
  providedIn: 'root'
})
export class DatafetcherService {

  constructor() { }

  fetchData(dataType: string): Object[]{
    if (dataType == null){
      return null
    }
    if(dataType.toLowerCase() === 'athletes'){
      return Athletes
    }
    else if(dataType.toLowerCase() === 'cars'){
      return Cars
    }
    else{
      return null;
    }
  }
}
