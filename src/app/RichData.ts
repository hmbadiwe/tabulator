export interface RichData{
    fields: string[];
    content: Object[];
}