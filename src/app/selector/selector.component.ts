import { Component, OnInit } from '@angular/core';
import {DatafetcherService} from '../datafetcher.service'

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent implements OnInit {

  dataTypes: string[] = ["Athletes", "Cars"]

  content: Object[]

  constructor(private dfs: DatafetcherService) { }

  ngOnInit(): void {
    this.content = this.dfs.fetchData(this.dataTypes[0]);
    console.log("content...")
    console.log(this.content)
  }

  onSelect(dataType: string): void{
    this.content = this.dfs.fetchData(dataType);
    console.log(this.content);
  }
}
