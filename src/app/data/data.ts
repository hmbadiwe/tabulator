export const Athletes: Object[] = [
    {
        name: 'Michael Jordan',
        sport: 'Basketball',
        ppg: 30.2,
        years: 14,
        mvps: 5
    },
    {
        name: 'Tom Brady',
        sport: 'Football',
        mvps: 3,
        years: '20+',
        totalYds: 74571
    },
    {
        name: 'Wayne Gretzy',
        sport: 'Ice Hockey',
        mvps: 9
    }
]
export const Cars: Object[] = [
    {
        maker: "Ford",
        firstYear: "1989",
        model: "F150",
        type: "Truck",
        transmission: "Automatic"
    },
    {
        maker: "BMW",
        firstYear: 1985,
        lastYear: 1991,
        model: "8 Series",
        type: "Sedan",
        transmission: "Manual",
        gears: 6
    },
    {
        maker: "Chrysler",
        firstYear: 1998,
        lastYear: 2009,
        model: "PT Cruiser",
        transmission: "Automatic"
    }
]