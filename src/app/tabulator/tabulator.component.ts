import { Component, OnInit, OnChanges } from '@angular/core';
import {Input } from '@angular/core'
@Component({
  selector: 'app-tabulator',
  templateUrl: './tabulator.component.html',
  styleUrls: ['./tabulator.component.css']
})
export class TabulatorComponent implements OnInit, OnChanges {

  @Input()
  data: [Object];

  tableColumns: String[]

  constructor() { }

  loadColumns(): void{
    let columns: String[] = []
    this.data.forEach(obj =>{
      let keys = Object.keys(obj);
      columns = columns.concat(keys);
    })
    let columnsSet = new Set(columns)
    this.tableColumns = Array.from(columnsSet)
  }
  ngOnInit(){
    this.loadColumns()
  }

  ngOnChanges(): void {
    this.loadColumns()
  }

}
